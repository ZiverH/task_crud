package az.ingress.lesson1;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
@RequiredArgsConstructor
public class Lesson1Application implements CommandLineRunner {

	@Value("${ms25.lesson}")
	private Integer lesson;
	@Value("${asan.url}")
	private String url;
	public static void main(String[] args) {
		SpringApplication.run(Lesson1Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("number is {}",lesson);
		log.info("url is {}",url);
	}
}
