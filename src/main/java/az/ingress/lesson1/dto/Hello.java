package az.ingress.lesson1.dto;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

@Data
@Slf4j
public class Hello {


    String name = "Hello";
    @PostConstruct
    public void init(){
        log.info("Bean is initialized");
    }
    @PreDestroy
    public void destroy(){
        log.info("Bean is destroyed");
    }
}
